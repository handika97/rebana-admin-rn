import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TouchableOpacity,
  Animated,
  Keyboard,
  TextInput,
  ScrollView,
  Pressable,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {authLoginReceive} from '../../redux/features/authSlice';
import {post, get} from '../../redux/features/main';
import {Loading} from '../../component/cell';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {FlatList} from 'react-native-gesture-handler';
import {BaseUrl} from '../../utilities/BaseUrl';
import * as ImagePicker from 'react-native-image-picker';

const ProductList = ({navigation, route}) => {
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  console.log(Auth.data);
  const [loading, setloading] = useState(false);
  const [errorMessage, seterrorMessage] = useState('');
  const [Error, setError] = useState(false);
  const [IdCategory, setIdCategory] = useState('all');
  const [Search, setSearch] = useState('');
  const [Data, setData] = useState([]);
  const [Category, setCategory] = useState([]);
  const [AddProduct, setAddProduct] = useState(false);
  const [imagePicker, setImagePicker] = useState(false);

  const [attribute, setAttribute] = useState({
    name: '',
    description: '',
    price: '',
    category: '',
    image: {uri: '', fileName: ''},
  });
  console.log(attribute);

  useEffect(() => {
    navigation.addListener('focus', () => {
      // Do whatever you want
      setloading(true);
      getData();
    });
  }, []);
  useEffect(() => {
    setloading(true);
    getData();
  }, [IdCategory, Search]);
  const getData = () => {
    dispatch(
      get(
        `/product/owner/${IdCategory}/${Auth.data.id}/${
          Search ? Search : 'null'
        }`,
        (res) => {
          console.log(res.data.data, 'data');
          setData(res.data.data);
        },
        (err) => {
          console.log(err);
          seterrorMessage(err.response.data.error_message);
          setError(true);
          setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
    dispatch(
      get(
        `/category/`,
        (res) => {
          setCategory(res.data.data);
        },
        (err) => {
          seterrorMessage(err.response.data.error_message);
          setError(true);
          setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
  };

  useEffect(() => {
    if (Search) {
      setIdCategory('all');
    }
  }, [Search]);

  const addProduct = () => {
    setloading(true);
    const dataFile = new FormData();
    dataFile.append('Image', {
      uri: attribute.image.uri,
      type: 'image/jpeg',
      name: attribute.image.fileName,
    });
    dataFile.append('id_user', Auth.data.id);
    dataFile.append('name', attribute.name);
    dataFile.append('description', attribute.description);
    dataFile.append('price', attribute.price);
    dataFile.append('category', attribute.category);
    dispatch(
      post(
        '/product/create',
        dataFile,
        (res) => {
          console.log(res);
          getData();
          setAttribute({
            name: '',
            description: '',
            price: '',
            category: '',
            image: {uri: '', fileName: ''},
          });
          setAddProduct(false);
        },
        (err) => {
          console.log(err.response);
          seterrorMessage(err.response.data.error_message);
          setError(true);
          setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#d9d9d9',
        borderTopRightRadius: 200,
      }}>
      <View style={{padding: 10, flex: 1}}>
        <Pressable
          style={{
            backgroundColor: 'white',
            elevation: 3,
            opacity: AddProduct ? 0.5 : 1,
            padding: 5,
            borderRadius: 5,
          }}
          onPress={() => navigation.navigate('User')}>
          <View style={{flexDirection: 'row'}}>
            {Auth.data.photo ? (
              <View
                style={{
                  backgroundColor: '#05c46b',
                  width: 55,
                  height: 55,
                  borderRadius: 100,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={{
                    uri: Auth.data.photo,
                  }}
                  style={{
                    height: '100%',
                    width: '100%',
                    resizeMode: 'cover',
                    borderRadius: 100,
                  }}
                />
              </View>
            ) : (
              <View
                style={{
                  backgroundColor: '#05c46b',
                  width: 55,
                  height: 55,
                  borderRadius: 100,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <FontAwesome5 name="user" size={20} color="white" />
              </View>
            )}
            <View style={{marginHorizontal: 10, justifyContent: 'center'}}>
              <Text
                style={{
                  fontSize: 14,
                  color: 'black',
                  fontWeight: '700',
                  textTransform: 'capitalize',
                }}>
                {Auth.data.store_name}
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  color: 'black',
                  textTransform: 'capitalize',
                }}>
                {Auth.data.address}
              </Text>
            </View>
          </View>
        </Pressable>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center',
          }}>
          <View style={{width: '70%', marginTop: -5}}>
            <Input
              value={Search}
              onChange={(e) => setSearch(e)}
              icons="search"
              placeholder="Cari"
            />
          </View>
          <Pressable
            style={{
              backgroundColor: '#05c46b',
              width: 30,
              height: 30,
              borderRadius: 100,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={() => setAddProduct(true)}>
            <FontAwesome5 name="plus" size={20} color="white" />
          </Pressable>
        </View>
        <View style={{height: 35, marginTop: 5}}>
          <ScrollView horizontal={true} style={{flex: 1}}>
            {Category &&
              Category.map((item) => {
                return (
                  <Pressable
                    style={{
                      padding: 5,
                      margin: 3,
                      height: 30,
                      borderRadius: 5,
                      borderWidth: 0.5,
                      borderColor: '#05c46b',
                      backgroundColor:
                        IdCategory === item.id ? '#05c46b' : 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                      elevation: 3,
                    }}
                    onPress={() =>
                      IdCategory === item.id
                        ? setIdCategory('all')
                        : setIdCategory(item.id)
                    }>
                    <Text
                      style={{
                        color: IdCategory === item.id ? 'white' : '#05c46b',
                        textTransform: 'capitalize',
                      }}>
                      {item.name}
                    </Text>
                  </Pressable>
                );
              })}
          </ScrollView>
        </View>
        <ScrollView style={{flex: 1, marginTop: 10}}>
          <FlatList
            data={Data}
            numColumns={2}
            renderItem={({item, index}) => {
              return (
                <Pressable
                  style={{
                    flex: 0.5,
                    justifyContent: 'flex-start',
                    alignItems: 'center',
                    marginBottom: 10,
                  }}
                  onPress={() => navigation.navigate('DetailProduct', item)}>
                  <View
                    style={{
                      backgroundColor: 'white',
                      padding: 2,
                      width: '95%',
                      heigth: 200,
                      elevation: 3,
                      borderRadius: 3,
                    }}>
                    <View
                      style={{
                        height: 100,
                        width: '100%',
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Image
                        source={{
                          uri: item.photo,
                        }}
                        style={{
                          height: '100%',
                          width: '100%',
                          resizeMode: 'contain',
                        }}
                      />
                    </View>
                    <Text
                      style={{
                        fontSize: 14.5,
                        color: 'black',
                        textTransform: 'capitalize',
                      }}>
                      {item.name}
                    </Text>
                    <Text
                      style={{
                        fontSize: 16,
                        color: '#05c46b',
                      }}>
                      Rp. {item.price}{' '}
                      <Text
                        style={{
                          color: item.status === 'ready' ? '#05c46b' : 'red',
                          fontSize: 14,
                        }}>
                        ({item.status})
                      </Text>
                    </Text>
                  </View>
                </Pressable>
              );
            }}
            keyExtractor={(item, index) => item.id}
          />
        </ScrollView>
      </View>

      {Error && (
        <View
          style={{
            backgroundColor: 'white',
            position: 'absolute',
            minWidth: '70%',
            maxWidth: '85%',
            padding: 10,
            top: 30,
            borderRadius: 3,
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text>{errorMessage}</Text>
        </View>
      )}
      {AddProduct && (
        <Pressable
          style={{
            position: 'absolute',
            zIndez: 10,
            backgroundColor: 'black',
            opacity: 0.5,
            height: '100%',
            width: '100%',
            flex: 1,
          }}
          onPress={() => setAddProduct(false)}
        />
      )}
      {AddProduct && (
        <View
          style={{
            position: 'absolute',
            zIndez: 3,
            backgroundColor: 'white',
            elevation: 3,
            height: 600,
            width: '100%',
            flex: 1,
            bottom: 0,
          }}>
          <Pressable style={{padding: 10}} onPress={() => setAddProduct(false)}>
            <FontAwesome5 name="times" size={20} />
          </Pressable>
          {!loading ? (
            <ScrollView>
              <View style={{padding: 10}}>
                <Pressable
                  style={{
                    height: 200,
                    width: '80%',
                    backgroundColor: '#05c46b',
                    elevation: 3,
                    borderRadius: 2,
                    padding: 2,
                    alignSelf: 'center',
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}
                  onPress={() => {
                    setImagePicker(true);
                    setAddProduct(false);
                  }}>
                  {attribute.image.uri ? (
                    <Image
                      source={{
                        uri: attribute.image.uri,
                      }}
                      style={{
                        height: '100%',
                        width: '100%',
                        resizeMode: 'cover',
                        borderRadius: 2,
                      }}
                    />
                  ) : (
                    <FontAwesome5 name="photo-video" size={50} color="black" />
                  )}
                </Pressable>
                <Input
                  placeholder="Name Product"
                  value={attribute.name}
                  icons="file-signature"
                  onChange={(e) => setAttribute({...attribute, name: e})}
                  width="70%"
                />
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: 'black',
                    marginTop: 20,
                    height: 100,
                    borderRadius: 2,
                  }}>
                  <TextInput
                    multiline={true}
                    numberOfLines={5}
                    value={attribute.description}
                    onChangeText={(e) =>
                      setAttribute({...attribute, description: e})
                    }
                    placeholder="Description Product"
                  />
                </View>
                <Input
                  placeholder="Price Product"
                  value={attribute.price}
                  type="numeric"
                  icons="money-bill-wave"
                  onChange={(e) => setAttribute({...attribute, price: e})}
                  width="70%"
                />
                <View style={{height: 35, marginTop: 5}}>
                  <ScrollView horizontal={true} style={{flex: 1}}>
                    {Category &&
                      Category.map((item) => {
                        return (
                          <Pressable
                            style={{
                              padding: 5,
                              margin: 3,
                              height: 30,
                              borderRadius: 5,
                              borderWidth: 0.5,
                              borderColor: '#05c46b',
                              backgroundColor:
                                attribute.category === item.id
                                  ? '#05c46b'
                                  : 'white',
                              justifyContent: 'center',
                              alignItems: 'center',
                              elevation: 3,
                            }}
                            onPress={() =>
                              setAttribute({
                                ...attribute,
                                category: item.id,
                              })
                            }>
                            <Text
                              style={{
                                color:
                                  attribute.category === item.id
                                    ? 'white'
                                    : '#05c46b',
                                textTransform: 'capitalize',
                              }}>
                              {item.name}
                            </Text>
                          </Pressable>
                        );
                      })}
                  </ScrollView>
                </View>
              </View>
              <View
                style={{
                  width: 150,
                  height: 70,
                  alignSelf: 'center',
                  marginTop: -20,
                }}>
                {attribute.name &&
                attribute.price &&
                attribute.description &&
                attribute.category &&
                attribute.image.uri ? (
                  <Button
                    title="Add Product"
                    color="#0fbcf9"
                    onPress={() => {
                      addProduct();
                    }}
                  />
                ) : null}
              </View>
            </ScrollView>
          ) : (
            <Loading color="#0fbcf9" text="Loading" />
          )}
        </View>
      )}
      {imagePicker && (
        <>
          <View
            style={{
              position: 'absolute',
              bottom: 30,
              flexDirection: 'row',
              alignSelf: 'center',
              backgroundColor: '#0fbcf9',
              paddingVertical: 5,
              paddingHorizontal: 10,
              borderRadius: 3,
              zIndex: 10,
            }}>
            <Pressable
              style={{padding: 5}}
              onPress={() => {
                setImagePicker(false);
                setAddProduct(true);
                ImagePicker.launchCamera(
                  {
                    mediaType: 'photo',
                    includeBase64: false,
                    maxHeight: 1000,
                    maxWidth: 1000,
                  },
                  (response) => {
                    console.log(response);
                    if (response.uri) {
                      setAttribute({
                        ...attribute,
                        image: {
                          uri: response.uri,
                          fileName: response.fileName,
                        },
                      });
                    }
                  },
                );
              }}>
              <FontAwesome5 name="camera" size={30} color="white" />
            </Pressable>
            <Pressable
              style={{padding: 5}}
              onPress={() => {
                setImagePicker(false);
                setAddProduct(true);
                ImagePicker.launchImageLibrary(
                  {
                    mediaType: 'photo',
                    includeBase64: false,
                    maxHeight: 1000,
                    maxWidth: 1000,
                  },
                  (response) => {
                    console.log(response);
                    if (response.uri) {
                      setAttribute({
                        ...attribute,
                        image: {
                          uri: response.uri,
                          fileName: response.fileName,
                        },
                      });
                    }
                  },
                );
              }}>
              <FontAwesome5 name="folder-open" size={30} color="white" />
            </Pressable>
          </View>
          <Pressable
            style={{
              position: 'absolute',
              zIndez: 9,
              backgroundColor: 'black',
              opacity: 0.5,
              height: '100%',
              width: '100%',
              flex: 1,
            }}
            onPress={() => setImagePicker(false)}
          />
        </>
      )}
    </View>
  );
};

export default ProductList;
