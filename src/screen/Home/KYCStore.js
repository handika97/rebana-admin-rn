import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  PermissionsAndroid,
  StyleSheet,
  Pressable,
  Image,
  Text,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
import {post, get, patch} from '../../redux/features/main';
import {Loading} from '../../component/cell';
import {authLoginReceive} from '../../redux/features/authSlice';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {BaseUrl} from '../../utilities/BaseUrl';
import * as ImagePicker from 'react-native-image-picker';
import Geolocation from 'react-native-geolocation-service';

const DetailProduct = ({navigation, route}) => {
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  const [errorMessage, seterrorMessage] = useState('');
  const [loading, setloading] = useState(false);
  const [LocationModal, setLocationModal] = useState(false);
  const [imagePicker, setImagePicker] = useState(false);
  const [Error, setError] = useState(false);
  const [EditProduct, setEditProduct] = useState(false);
  const [Category, setCategory] = useState([]);
  const [attribute, setAttribute] = useState({
    image: {uri: '', fileName: ''},
  });
  const [LocationNow, setLocationNow] = useState({});
  const [location, setLocation] = useState({});

  //   const dispatch = useDispatch();
  useEffect(() => {
    Permision();
  }, []);

  const Permision = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Izinkan Aplikasi Mengakses Lokasi Anda',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
        PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION,
        {
          title: 'Izinkan Aplikasi Mengakses Lokasi Anda',
          buttonNeutral: 'Ask Me Later',
          buttonNegative: 'Cancel',
          buttonPositive: 'OK',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Geolocation.getCurrentPosition(
          (position) => {
            setLocation({
              latitude: position.coords.latitude,
              longitude: position.coords.longitude,
              latitudeDelta: 0.008253919629830797,
              longitudeDelta: 0.004210062325014974,
            });
            setLocationNow(position.coords);
          },
          (error) => {
            // See error code charts below.
            console.log(error.code, error.message);
          },
          {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
        );
      } else {
        alert('Aplikasi Membutuhkan Lokasi Anda');
      }
    } catch (err) {
      console.warn(err);
    }
  };

  const editPhotoProduct = () => {
    console.log('yee');
    const dataFile = new FormData();
    dataFile.append('Image', {
      uri: attribute.image.uri,
      type: 'image/jpeg',
      name: attribute.image.fileName,
    });
    dataFile.append('id', Auth.data.id);
    dataFile.append('longitude', LocationNow.longitude);
    dataFile.append('latitude', LocationNow.latitude);
    dispatch(
      post(
        '/kyc/uploadphoto',
        dataFile,
        (res) => {
          dispatch(authLoginReceive(res.data.data));
          seterrorMessage('Update Berhasil');
        },
        (err) => {
          console.log(err);
          seterrorMessage(err.response.data.error_message);
          setError(true);
          setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#d9d9d9',
        borderTopRightRadius: 200,
        paddingVertical: 20,
        alignItems: 'center',
        height: '100%',
      }}>
      <Pressable
        style={{
          backgroundColor: '#05c46b',
          width: 100,
          height: 100,
          borderRadius: 100,
          justifyContent: 'center',
          alignItems: 'center',
          marginVertical: 20,
        }}
        onPress={() => setImagePicker(true)}>
        {attribute.image.uri ? (
          <>
            <Image
              source={{
                uri: attribute.image.uri,
              }}
              style={{
                height: '100%',
                width: '100%',
                resizeMode: 'cover',
                borderRadius: 100,
              }}
            />
            <View
              style={{
                position: 'absolute',
                backgroundColor: '#0fbcf9',
                width: 30,
                height: 30,
                borderRadius: 100,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'flex-end',
                bottom: 0,
              }}>
              <FontAwesome5 name="camera" size={20} color="black" />
            </View>
          </>
        ) : (
          <>
            <FontAwesome5 name="user" size={40} color="white" />
            <View
              style={{
                position: 'absolute',
                backgroundColor: '#0fbcf9',
                width: 30,
                height: 30,
                borderRadius: 100,
                justifyContent: 'center',
                alignItems: 'center',
                alignSelf: 'flex-end',
                bottom: 0,
              }}>
              <FontAwesome5 name="camera" size={20} color="black" />
            </View>
          </>
        )}
      </Pressable>
      <Text style={{fontSize: 20}}>Lokasi Toko</Text>
      {location.latitude ? (
        <View style={{width: '95%', height: 400}}>
          <MapView
            provider={PROVIDER_GOOGLE} // remove if not using Google Maps
            style={styles.map}
            onRegionChange={(e) => {
              setLocationNow(e), console.log(e);
            }}
            initialRegion={location}></MapView>
          <View
            style={{
              height: '100%',
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
              position: 'absolute',
            }}>
            <FontAwesome5
              name="map-marker-alt"
              size={35}
              color="purple"
              style={{bottom: 10}}
            />
          </View>
        </View>
      ) : null}
      <View
        style={{
          width: 150,
          height: 70,
          alignSelf: 'center',
          marginTop: -20,
          marginBottom: 20,
        }}>
        {attribute.image.uri && LocationNow.latitude ? (
          <View style={{opacity: imagePicker ? 0.5 : 1}}>
            <Button
              title="Update Info"
              color="#0fbcf9"
              onPress={() => {
                editPhotoProduct();
                console.log('yee');
              }}
            />
          </View>
        ) : null}
      </View>
      {imagePicker && (
        <>
          <View
            style={{
              position: 'absolute',
              bottom: 30,
              flexDirection: 'row',
              alignSelf: 'center',
              backgroundColor: '#0fbcf9',
              paddingVertical: 5,
              paddingHorizontal: 10,
              borderRadius: 3,
              zIndex: 10,
            }}>
            <Pressable
              style={{padding: 5}}
              onPress={() => {
                setImagePicker(false);
                ImagePicker.launchCamera(
                  {
                    mediaType: 'photo',
                    includeBase64: false,
                    maxHeight: 1000,
                    maxWidth: 1000,
                  },
                  (response) => {
                    console.log(response);
                    if (response.uri) {
                      setAttribute({
                        ...attribute,
                        image: {
                          uri: response.uri,
                          fileName: response.fileName,
                        },
                      });
                    }
                  },
                );
              }}>
              <FontAwesome5 name="camera" size={30} color="white" />
            </Pressable>
            <Pressable
              style={{padding: 5}}
              onPress={() => {
                setImagePicker(false);
                ImagePicker.launchImageLibrary(
                  {
                    mediaType: 'photo',
                    includeBase64: false,
                    maxHeight: 1000,
                    maxWidth: 1000,
                  },
                  (response) => {
                    console.log(response);
                    if (response.uri) {
                      setAttribute({
                        ...attribute,
                        image: {
                          uri: response.uri,
                          fileName: response.fileName,
                        },
                      });
                    }
                  },
                );
              }}>
              <FontAwesome5 name="folder-open" size={30} color="white" />
            </Pressable>
          </View>
          <Pressable
            style={{
              position: 'absolute',
              zIndez: 9,
              backgroundColor: 'black',
              opacity: 0.5,
              height: '100%',
              width: '100%',
              flex: 1,
            }}
            onPress={() => setImagePicker(false)}
          />
        </>
      )}
    </View>
  );
};

export default DetailProduct;
const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});
