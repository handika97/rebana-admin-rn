import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Image,
  Text,
  TextInput,
  ScrollView,
  Pressable,
} from 'react-native';
import {Input, Button} from '../../component/cell';
import {useDispatch, useSelector} from 'react-redux';
import {authLoginReceive} from '../../redux/features/authSlice';
import {post, get, patch, deleted} from '../../redux/features/main';
import {Loading} from '../../component/cell';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {FlatList} from 'react-native-gesture-handler';
import {BaseUrl} from '../../utilities/BaseUrl';
import * as ImagePicker from 'react-native-image-picker';

const Status = [
  {id: 'ready', name: 'ready'},
  {id: 'empty', name: 'empty'},
];

const DetailProduct = ({navigation, route}) => {
  const item = route.params;
  const dispatch = useDispatch();
  const Auth = useSelector((state) => state.auth);
  const [imagePicker, setImagePicker] = useState(false);
  const [errorMessage, seterrorMessage] = useState('');
  const [loading, setloading] = useState(false);
  const [LocationModal, setLocationModal] = useState(false);
  const [Error, setError] = useState(false);
  const [EditProduct, setEditProduct] = useState(false);
  const [Category, setCategory] = useState([]);
  const [attribute, setAttribute] = useState({
    name: item.name,
    description: item.description,
    price: item.price,
    category: item.category,
    status: item.status,
    image: {uri: '', fileName: ''},
    photo: item.photo,
  });
  useEffect(() => {
    dispatch(
      get(
        `/category/`,
        (res) => {
          setCategory(res.data.data);
        },
        (err) => {
          seterrorMessage(err.response.data.error_message);
          setError(true);
          setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
  }, []);

  const editProduct = () => {
    setloading(true);
    dispatch(
      patch(
        '/product/update',
        {
          name: attribute.name,
          description: attribute.description,
          price: attribute.price,
          category: attribute.category,
          status: attribute.status,
          id: item.id,
        },
        (res) => {
          console.log(res.data.data);
          // item = res.data.data;
          setAttribute({
            ...attribute,
            name: res.data.data.name,
            description: res.data.data.description,
            price: res.data.data.price,
            category: res.data.data.category,
            status: res.data.data.status,
          });
          setEditProduct(false);
        },
        (err) => {
          // seterrorMessage(err.response.data.error_message);
          // setError(true);
          // setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
  };

  const editPhotoProduct = (response) => {
    console.log(response);
    const dataFile = new FormData();
    dataFile.append('Image', {
      uri: response.uri,
      type: 'image/jpeg',
      name: response.fileName,
    });
    dataFile.append('id', item.id);
    dispatch(
      patch(
        '/product/updatephoto',
        dataFile,
        (res) => {
          setAttribute({
            ...attribute,
            image: {uri: '', fileName: ''},
            photo: res.data.data.photo,
          });
        },
        (err) => {
          console.log(err);
          seterrorMessage(err.response.data.error_message);
          setError(true);
          setTimeout(() => setError(false), 3000);
        },
        () => {
          setloading(false);
        },
      ),
    );
  };

  const deleteProduct = () => {
    dispatch(
      deleted(
        '/product/' + item.id,
        (res) => {
          navigation.goBack();
        },
        (err) => {
          alert('error');
        },
        () => {},
      ),
    );
  };

  return (
    <View
      style={{
        flex: 1,
        backgroundColor: '#d9d9d9',
        borderTopRightRadius: 200,
      }}>
      <ScrollView
        style={{
          flex: 1,
          height: '100%',
          zIndez: 0,
          width: '100%',
        }}>
        <Pressable style={{padding: 10}} onPress={() => navigation.goBack()}>
          <FontAwesome5 name="arrow-left" size={20} />
        </Pressable>
        <Pressable
          style={{
            height: 300,
            width: '100%',
            justifyContent: 'center',
            alignItems: 'center',
            padding: 5,
          }}
          onPress={() => setImagePicker(true)}>
          <View
            style={{
              height: 300,
              width: '100%',
              backgroundColor: 'white',
              elevation: 3,
              borderRadius: 2,
              padding: 2,
            }}>
            <Image
              source={{
                uri: attribute.photo,
              }}
              style={{
                height: '100%',
                width: '100%',
                resizeMode: 'contain',
                borderRadius: 2,
              }}
            />
          </View>
        </Pressable>
        <View
          style={{
            paddingHorizontal: 5,
            marginTop: 5,
          }}>
          <View
            style={{
              backgroundColor: 'white',
              elevation: 3,
              padding: 5,
              borderRadius: 5,
            }}>
            <Text
              style={{
                fontSize: 17,
                color: 'black',
                textTransform: 'capitalize',
                marginBottom: 5,
                fontWeight: 'bold',
              }}>
              {attribute.name}
            </Text>
            <Text
              style={{
                fontSize: 16,
                color: '#05c46b',
                fontWeight: 'bold',
              }}>
              Rp. {attribute.price}
            </Text>
          </View>
        </View>
        <View style={{paddingHorizontal: 5, marginTop: 5}}>
          <View
            style={{
              backgroundColor: 'white',
              elevation: 3,
              padding: 5,
              borderRadius: 5,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View
              style={{
                flexWrap: 'wrap',
                flex: 0.8,
              }}>
              <Text
                style={{
                  fontSize: 15,
                  color: attribute.status === 'ready' ? '#05c46b' : 'red',
                  fontWeight: 'bold',
                  textTransform: 'capitalize',
                }}>
                {attribute.status}
              </Text>
              <Text
                style={{
                  fontSize: 14,
                  color: 'black',
                  textTransform: 'capitalize',
                  flexWrap: 'wrap',
                  maxWidth: '100%',
                }}>
                {attribute.description}
              </Text>
            </View>
            <Pressable style={{padding: 10}} onPress={() => deleteProduct()}>
              <FontAwesome5 name="trash" size={20} color="red" />
            </Pressable>
          </View>
        </View>
        <View
          style={{
            width: 150,
            height: 70,
            alignSelf: 'center',
            marginTop: -20,
            zIndez: 0,
          }}>
          <Button
            title="Edit Product"
            color="#0fbcf9"
            onPress={() => {
              setEditProduct(true);
            }}
          />
        </View>
        {Error && (
          <View
            style={{
              backgroundColor: 'white',
              elevation: 3,
              position: 'absolute',
              minWidth: '70%',
              maxWidth: '85%',
              padding: 10,
              top: 30,
              borderRadius: 5,
              alignSelf: 'center',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text>{errorMessage}</Text>
          </View>
        )}
      </ScrollView>
      {EditProduct && (
        <Pressable
          style={{
            position: 'absolute',
            zIndez: 10,
            backgroundColor: 'black',
            opacity: 0.5,
            height: '100%',
            width: '100%',
            flex: 1,
          }}
          onPress={() => setEditProduct(false)}
        />
      )}
      {EditProduct && (
        <View
          style={{
            position: 'absolute',
            zIndez: 3,
            backgroundColor: 'white',
            elevation: 3,
            height: 400,
            width: '100%',
            flex: 1,
            bottom: 0,
          }}>
          <Pressable
            style={{padding: 10}}
            onPress={() => setEditProduct(false)}>
            <FontAwesome5 name="times" size={20} />
          </Pressable>
          {!loading ? (
            <ScrollView>
              <View style={{padding: 10}}>
                <Input
                  placeholder="Name Product"
                  value={attribute.name}
                  icons="file-signature"
                  onChange={(e) => setAttribute({...attribute, name: e})}
                  width="70%"
                />
                <View
                  style={{
                    borderWidth: 1,
                    borderColor: 'black',
                    marginTop: 20,
                    height: 100,
                    borderRadius: 2,
                  }}>
                  <TextInput
                    multiline={true}
                    numberOfLines={5}
                    value={attribute.description}
                    onChangeText={(e) =>
                      setAttribute({...attribute, description: e})
                    }
                    placeholder="Description Product"
                  />
                </View>
                <Input
                  placeholder="Price Product"
                  value={`${attribute.price}`}
                  type="numeric"
                  icons="money-bill-wave"
                  onChange={(e) => setAttribute({...attribute, price: e})}
                  width="70%"
                />
                <View style={{height: 35, marginTop: 5}}>
                  <ScrollView horizontal={true} style={{flex: 1}}>
                    {Category &&
                      Category.map((item) => {
                        return (
                          <Pressable
                            style={{
                              padding: 5,
                              margin: 3,
                              height: 30,
                              borderRadius: 5,
                              borderWidth: 0.5,
                              borderColor: '#05c46b',
                              backgroundColor:
                                attribute.category === item.id
                                  ? '#05c46b'
                                  : 'white',
                              justifyContent: 'center',
                              alignItems: 'center',
                              elevation: 3,
                            }}
                            onPress={() =>
                              setAttribute({
                                ...attribute,
                                category: item.id,
                              })
                            }>
                            <Text
                              style={{
                                color:
                                  attribute.category === item.id
                                    ? 'white'
                                    : '#05c46b',
                                textTransform: 'capitalize',
                              }}>
                              {item.name}
                            </Text>
                          </Pressable>
                        );
                      })}
                  </ScrollView>
                </View>
                <View style={{height: 35, marginTop: 5}}>
                  <ScrollView horizontal={true} style={{flex: 1}}>
                    {Status &&
                      Status.map((item) => {
                        return (
                          <Pressable
                            style={{
                              padding: 5,
                              margin: 3,
                              height: 30,
                              borderRadius: 5,
                              borderWidth: 0.5,
                              borderColor: '#05c46b',
                              backgroundColor:
                                attribute.status === item.id
                                  ? '#05c46b'
                                  : 'white',
                              justifyContent: 'center',
                              alignItems: 'center',
                              elevation: 3,
                            }}
                            onPress={() =>
                              setAttribute({
                                ...attribute,
                                status: item.id,
                              })
                            }>
                            <Text
                              style={{
                                color:
                                  attribute.status === item.id
                                    ? 'white'
                                    : '#05c46b',
                                textTransform: 'capitalize',
                              }}>
                              {item.name}
                            </Text>
                          </Pressable>
                        );
                      })}
                  </ScrollView>
                </View>
              </View>
              <View
                style={{
                  width: 150,
                  height: 70,
                  alignSelf: 'center',
                  marginTop: -20,
                  marginBottom: 20,
                }}>
                {attribute.name &&
                attribute.price &&
                attribute.description &&
                attribute.category &&
                attribute.status ? (
                  <Button
                    title="Update"
                    color="#0fbcf9"
                    onPress={() => {
                      editProduct();
                    }}
                  />
                ) : null}
              </View>
            </ScrollView>
          ) : (
            <Loading color="#0fbcf9" text="Loading" />
          )}
        </View>
      )}
      {imagePicker && (
        <>
          <View
            style={{
              position: 'absolute',
              bottom: 30,
              flexDirection: 'row',
              alignSelf: 'center',
              backgroundColor: '#0fbcf9',
              paddingVertical: 5,
              paddingHorizontal: 10,
              borderRadius: 3,
              zIndex: 10,
            }}>
            <Pressable
              style={{padding: 5}}
              onPress={() => {
                setImagePicker(false);
                ImagePicker.launchCamera(
                  {
                    mediaType: 'photo',
                    includeBase64: false,
                    maxHeight: 1000,
                    maxWidth: 1000,
                  },
                  (response) => {
                    console.log(response);
                    if (response.uri) {
                      editPhotoProduct(response);
                    }
                  },
                );
              }}>
              <FontAwesome5 name="camera" size={30} color="white" />
            </Pressable>
            <Pressable
              style={{padding: 5}}
              onPress={() => {
                setImagePicker(false);
                ImagePicker.launchImageLibrary(
                  {
                    mediaType: 'photo',
                    includeBase64: false,
                    maxHeight: 1000,
                    maxWidth: 1000,
                  },
                  (response) => {
                    console.log(response);
                    if (response.uri) {
                      editPhotoProduct(response);
                    }
                  },
                );
              }}>
              <FontAwesome5 name="folder-open" size={30} color="white" />
            </Pressable>
          </View>
          <Pressable
            style={{
              position: 'absolute',
              zIndez: 9,
              backgroundColor: 'black',
              opacity: 0.5,
              height: '100%',
              width: '100%',
              flex: 1,
            }}
            onPress={() => setImagePicker(false)}
          />
        </>
      )}
    </View>
  );
};

export default DetailProduct;
