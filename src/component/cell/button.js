import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  TextInput,
  Pressable,
} from 'react-native';

const Button = ({title, onPress = () => {}, small, style, color}) => {
  return (
    <TouchableOpacity
      style={{
        height: small ? '100%' : 40,
        backgroundColor: color ? color : 'purple',
        top: small ? 0 : 30,
        borderRadius: 5,
        elevation: 2,
        zIndez: 0,
        alignItems: 'center',
        justifyContent: 'center',
        ...style,
      }}
      onPress={() => onPress()}>
      <Text style={{color: 'white', fontSize: small ? 13 : 20}}>{title}</Text>
    </TouchableOpacity>
  );
};

export default Button;
