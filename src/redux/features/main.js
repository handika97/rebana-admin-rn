import {createSlice} from '@reduxjs/toolkit';
import Axios from 'axios';
import {BaseUrl} from '../../utilities/BaseUrl';
// import reactotron from 'reactotron-react-native';
export const slice = createSlice({
  name: 'main',
  initialState: {},
  reducers: {},
});

export const {} = slice.actions;

export const post = (
  link,
  data,
  // ApiClient,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {},
) => (dispatch, getState) => {
  const {auth} = getState();
  console.log(BaseUrl + link);
  Axios.post(BaseUrl + link, data, {
    headers: auth.headers,
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};
export const patch = (
  link,
  data,
  // ApiClient,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {},
) => (dispatch, getState) => {
  const {auth} = getState();
  Axios.patch(BaseUrl + link, data, {
    headers: auth.headers,
  })
    .then((res) => {
      ifSuccess(res);
      console.log(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};
export const get = (
  link,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {},
) => (dispatch, getState) => {
  const {auth} = getState();
  console.log(link);
  Axios.get(BaseUrl + link, {
    headers: auth.headers,
  })
    .then((res) => {
      ifSuccess(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};
export const deleted = (
  link,
  // ApiClient,
  ifSuccess = () => {},
  ifError = () => {},
  finallyDo = () => {},
) => (dispatch, getState) => {
  const {auth} = getState();
  Axios.delete(BaseUrl + link, {
    headers: auth.headers,
    data: {},
  })
    .then((res) => {
      ifSuccess(res);
      console.log(res);
    })
    .catch((err) => {
      ifError(err);
    })
    .finally(() => {
      finallyDo();
    });
};

export default slice.reducer;
