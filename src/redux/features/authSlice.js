import {createSlice} from '@reduxjs/toolkit';
// import Geolocation from 'react-native-geolocation-service';
import {BaseUrl} from '../../utilities/BaseUrl';
const authSlice = createSlice({
  name: 'auth',
  initialState: {
    Loading: false,
    data: {
      id: '',
      name: '',
      email: '',
      VerifyAccount: '',
      createdAt: '',
      updatedAt: '',
      token: '',
    },
    error: null,
    isLogin: false,
    headers: {
      token: '',
      'Content-Type': 'application/json',
    },
  },
  reducers: {
    authLoginPending: (state) => {
      state.Loading = true;
    },
    authLoginRejected: (state, action) => {
      state.Loading = false;
      state.error = action.payload;
    },
    authLoginReceive: (state, action) => {
      state.isLogin = true;
      state.Loading = false;
      state.data = action.payload;
      state.headers.token = action.payload.token;
    },
    onErrorRead: (state) => {
      state.error = null;
    },
    authLogOut: (state) => {
      state.isLogin = false;
      state.data = '';
      state.headers.token = '';
    },
  },
});

export const {
  authLoginPending,
  authLoginRejected,
  authLoginReceive,
  authLogOut,
} = authSlice.actions;

export default authSlice.reducer;
import {post} from './main';
const defaultBody = null;

export const authLogin = (email, password, error = () => {}) => {
  let bodyData = {
    email: email,
    password: password,
  };
  return (dispatch) => {
    dispatch(authLoginPending());
    dispatch(
      post(
        BaseUrl + '/auth/login',
        bodyData,
        (res) => {
          console.log(res.data, 'ini res');
          dispatch(authLoginReceive(res.data));
        },
        (err) => {
          console.log(err.response.data.message);
          dispatch(authLoginRejected(err.response.data.message));
          error();
        },
      ),
    );
  };
};

export const authRegister = (name, email, password) => {
  let bodyData = {
    name: name,
    email: email,
    password: password,
  };
  console.log(bodyData);
  return (dispatch) => {
    dispatch(
      post(
        '/admin/register',
        bodyData,
        (res) => {
          alert('success');
          console.log(res);
        },
        (err) => {
          alert('err');
          console.log(err);
        },
      ),
    );
  };
};

export const onErrorReset = () => {
  return (dispatch) => {
    dispatch(onErrorRead());
  };
};
